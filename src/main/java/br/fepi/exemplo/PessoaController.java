package br.fepi.exemplo;

import java.util.ArrayList;
import java.util.List;

import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController()
@RequestMapping("/pessoas")
public class PessoaController {
    
    //lista criada para simular um banco de dados
    private ArrayList<Pessoa> listaPessoas = new ArrayList<>();
    
    public PessoaController(){
        listaPessoas.add(new Pessoa("João", "Lucas", 25, "joao@teste.com"));
        listaPessoas.add(new Pessoa("Jose", "Lucas", 30, "jose@teste.com"));
        listaPessoas.add(new Pessoa("Maria", "Silva", 40, "maria@teste.com"));
    }

    //método que retorna a lista de todas as pessoas
    @GetMapping("/todos")
    public List<Pessoa> listarPessoas(){
        return listaPessoas;
    }


    //apenas um exemplo mostrando o uso de variáveis na rota
    @GetMapping("/hello/{sobrenome}")
	public String ola(@PathVariable String sobrenome){
		return "Ola, "+ sobrenome + "! Bem vindo a sua aplicação!";	
	}

    //mais um exemplo para demonstrar o uso de variáveis de path
    @GetMapping("/quadrado/{num}")
    public String quadrado(@PathVariable int num){
        return "Resultado: "+ num*num;
    }

    //exemplo de uso de variável na rota (faz um filtro para retornar apenas pessoas com determinado sobrenome)
    @GetMapping("/sobrenome/{sobrenome}")
    public ArrayList<Pessoa> obterPessoa(@PathVariable String sobrenome){

        ArrayList<Pessoa> lista = new ArrayList<>();

        for (Pessoa pessoa : listaPessoas) {
            if(pessoa.getSobrenome().equals(sobrenome)){
                lista.add(pessoa);
            }
        }

        return lista;
    }


    @DeleteMapping("/{indice}")
    public Pessoa delete(@PathVariable int indice){
        return listaPessoas.remove(indice);
    }

    //deve ser adicionado no corpo da requisição um objeto JSON para representar Pessoa
    @PostMapping()
    public ArrayList<Pessoa> inserir(@RequestBody Pessoa pessoa){
        listaPessoas.add(pessoa);
        return listaPessoas;
    }
}
